<p align="center"><img src="https://gitlab.com/NovumGit/innovation-app-core/-/raw/master/assets/novum.png"  alt="Novum logo"/></p>

# Novum Innovation app demo
This repository contains a bunch of demo API's that may be used to play with the Novum innovation app.

- [Before installing / requirements](/docs/pre-install.md)
- [Installation](/docs/installation.md)
