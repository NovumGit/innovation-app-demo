# Pre install requirements
This system requires you to have:

1. A server / development machine with Docker and Docker-compose installed. 
2. Access to a DNS server or your hosts file to add the required (sub) domains.
3. The git client needs to be installed.

---
[Proceed to installation &raquo;](/docs/installation.md)
