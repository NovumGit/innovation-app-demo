# Installation
After you took care that the [installation requirements](/docs/installation.md) are met you can run the demo application 
or create your own.

## 1. Get the code
``git clone git@gitlab.com:NovumGit/innovatieapp.git``

This will get you an example project that you can use to create your own. 

## 2. Configure hosts file
The demo consists of an API and an Admin panel / UI. In order to access them you need to add a DNS entry for each system
in your local hosts file. The IP address of the Docker container that runs the webserver is set to 172.18.0.2. The DNS
names that we chose are **admin.overheid.demo.novum.nuidev.nl** and api.overheid.demo.novum.nuidev.nl so your hosts file
should contain something like:

```
172.18.0.2  admin.overheid.demo.novum.nuidev.nl
172.18.0.2  api.overheid.demo.novum.nuidev.nl
```

## 3. Build the demo
From the root of the git repository that you just downloaded cd into docker and call the run and build commands.
```
docker-compose build
docker-compose up -d
```

## 4. Demo running
You can now open a webbrowser and play around with the system by going to any of these systems

[http://api.overheid.demo.novum.nuidev.nl](http://api.overheid.demo.novum.nuidev.nl)<br>
[http://admin.overheid.demo.novum.nuidev.nl](http://admin.overheid.demo.novum.nuidev.nl)

Login details for the admin panel:<br>
User: **admin@overheid.nl**<br />
Pass: **admin**<br />
(You will be asked to change your password right after signing in)

